using System.Collections;
using UnityEngine;

namespace VostokVR.FiveWalls {

    public class LightColorRandomizer : MonoBehaviour {

        [Space( 10 ), SerializeField]
        private Light controlling_light;

        [SerializeField]
        private Color initial_color;

        [SerializeField, Range( 0.1f, 10f )]
        private float loop_time = 1;


		/// <summary>
		///  Start is called before first frame rendering.
		/// </summary>
		private IEnumerator Start() {
			
            while( enabled ) {

                yield return new WaitForSeconds( loop_time );

                Color color = new Color( 
                    
                    Random.Range( 0.1f, 1f ), 
                    Random.Range( 0.1f, 1f ), 
                    Random.Range( 0.1f, 1f ),
                    1f
                );

                controlling_light.color = color;
            }

            yield break;
		}


		/// <summary>
		///  OnEnable is called before first frame rendering.
		/// </summary>
		private void OnEnable() {

            controlling_light.color = initial_color;
        }


        /// <summary>
        ///  OnDisable is called after last frame rendering.
        /// </summary>
        void OnDisable() {

            controlling_light.color = initial_color;
        }
    }
 }
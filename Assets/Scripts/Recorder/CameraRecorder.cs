using System.IO;
using UnityEngine;
using UnityEditor.Recorder;
using UnityEditor.Recorder.Encoder;
using UnityEditor.Recorder.Input;
using System;

namespace VostokVR.FiveWalls {

    #if( UNITY_EDITOR )
    public class CameraRecorder : MonoBehaviour {

        [Space( 10 ), SerializeField]
        private RecordingControl recording_control;

        private RecorderController recorder_controller;

        internal MovieRecorderSettings recorder_settings = null;

        public FileInfo OutputFile => new FileInfo( recorder_settings.OutputFile + ".mp4" );


        /// <summary>
        ///  OnEnable is called before first frame rendering.
        /// </summary>
        private void OnEnable() {

            Initialize();
        }


        /// <summary>
        /// OnDisable called after last frame rendering.
        /// </summary>
        private void OnDisable() {

            recorder_controller?.StopRecording();
            
            StopRecording();
        }

        /// <summary>
        /// Initializes the recorder.
        /// </summary>
        internal void Initialize() {

            recording_control.Initialize();

            RecorderControllerSettings controller_settings = ScriptableObject.CreateInstance<RecorderControllerSettings>();

            recorder_controller = new RecorderController( controller_settings );

            DirectoryInfo output_folder = new DirectoryInfo( Path.Combine( Application.dataPath, "StreamingAssets/Recordings" ) );

            string filename = MakeFilename();

            Vector2Int frame_resolution = recording_control.GetFrameResolution();

            recorder_settings = ScriptableObject.CreateInstance<MovieRecorderSettings>();
            recorder_settings.name = gameObject.tag;
            recorder_settings.CaptureAudio = recording_control.Record_audio;
            recorder_settings.Enabled = true;
            recorder_settings.CaptureAlpha = true;
            recorder_settings.OutputFile = output_folder.FullName + "/" + filename;

            recorder_settings.EncoderSettings = new CoreEncoderSettings {

                EncodingQuality = CoreEncoderSettings.VideoEncodingQuality.High,
                Codec = CoreEncoderSettings.OutputCodec.MP4
            };

            recorder_settings.ImageInputSettings = new GameViewInputSettings {

                OutputWidth = frame_resolution.x,
                OutputHeight = frame_resolution.y
            };

            if( File.Exists( OutputFile.FullName ) ) {

                try { 
                
                    File.Delete( OutputFile.FullName );
                }

                catch { 
                    
                    Debug.LogError( "Cannot delete file " + OutputFile.FullName + " before recording!" );
                }
            }

            controller_settings.AddRecorderSettings( recorder_settings );
            controller_settings.SetRecordModeToManual();
            controller_settings.FrameRate = recording_control.GetFrameRate();

            RecorderOptions.VerboseMode = false;
            recorder_controller.PrepareRecording();
            recorder_controller.StartRecording();

            Debug.Log( $"Recording started for file {OutputFile.FullName}" );
        }


        /// <summary>
        /// Returns a specific filename.
        /// </summary>
        private string MakeFilename() { 
            
            string filename = name + "-";

            filename += DateTime.Now.Year + "-";
            filename += ((DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month : DateTime.Now.Month) + "-";
            filename += ((DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day : DateTime.Now.Day) + "-";
            filename += (recording_control.Take < 10) ? "0" + recording_control.Take : recording_control.Take;

            return filename;
        }


        /// <summary>
        /// Deinitializes recording.
        /// </summary>
        private void StopRecording() { 
        
            if( (Camera.main != null) && !Camera.main.gameObject.activeSelf ) {

                Camera.main.gameObject.SetActive( true );
            }

            Debug.Log( "Recorded video has been saved for " + name );

            gameObject.SetActive( false );
        }
    }
    #endif
}
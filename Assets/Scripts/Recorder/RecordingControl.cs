using UnityEditor.Recorder;
using UnityEngine;

namespace VostokVR.FiveWalls {

    #if( UNITY_EDITOR )
    public enum VideoQuality { 
    
        _HD_720p,
        _FHD_1080p,
        _QHD_1440p,
        _4K_2160p,
        _5K_2880p,
        _8K_4320p
    }

    public enum FrameRate { 
    
        _30 = 30,
        _60 = 60
    }

    public class RecordingControl : MonoBehaviour {

        private RecordingControl instance;
        public RecordingControl Instance => (instance == null) ? (instance = FindObjectOfType<RecordingControl>( true )) :  instance;

        [Space( 10 ), SerializeField]
        private VideoQuality video_quality = VideoQuality._4K_2160p;

        [SerializeField]
        private FrameRate frame_rate = FrameRate._30;

        [SerializeField]
        private bool record_audio = false;
        public bool Record_audio => record_audio;

        [Space( 10 ), SerializeField]
        private CameraRecorder[] camera_recorders;

        private string take_key = "Take";

        public int Take { get; private set; } = 0;


        /// <summary>
        /// Start is called before the first frame update.
        /// </summary>
        private void OnEnable() {

            instance = this;

            for( int i = 0; i < camera_recorders.Length; i++ ) { 
                
                camera_recorders[i].gameObject.SetActive( true );
            }
        
            Camera.main?.gameObject.SetActive( false );
        }


        /// <summary>
        /// OnDisable called after last frame rendering.
        /// </summary>
        private void OnDisable() {

            PlayerPrefs.SetInt( take_key, Take );
            PlayerPrefs.Save();
        }


        /// <summary>
        /// Initializes a take.
        /// </summary>
        public void Initialize() {

            if( PlayerPrefs.HasKey( take_key ) ) { 
            
                Take = PlayerPrefs.GetInt( take_key, 0 ) + 1;
            }

            else { 
            
                Take += 1;
            }
        }


        /// <summary>
        /// Retirns a frame resolution depending on selected parameters.
        /// </summary>
        public Vector2Int GetFrameResolution() { 

            Vector2Int resolution = Vector2Int.zero;
            
            switch( video_quality ) {

                case VideoQuality._HD_720p: { 
                
                    resolution.x = 720;
                    resolution.y = 480;

                    break;
                }

                case VideoQuality._FHD_1080p: { 
                
                    resolution.x = 1920;
                    resolution.y = 1080;

                    break;
                }

                case VideoQuality._QHD_1440p: { 
                
                    resolution.x = 2560;
                    resolution.y = 1440;

                    break;
                }

                case VideoQuality._4K_2160p: { 
                
                    resolution.x = 3840;
                    resolution.y = 2160;

                    break;
                }

                case VideoQuality._5K_2880p: { 
                
                    resolution.x = 5120;
                    resolution.y = 2880;

                    break;
                }

                case VideoQuality._8K_4320p: { 
                
                    resolution.x = 7680;
                    resolution.y = 4320;

                    break;
                }

                default: { 
                
                    resolution.x = 720;
                    resolution.y = 480;

                    break;
                }
            }

            return resolution;
        }


        /// <summary>
        /// Returns a current framerate.
        /// </summary>
        public int GetFrameRate() { 
            
            int framerate = (int) frame_rate;

            return framerate;
        }


        /// <summary>
        /// Disables recording.
        /// </summary>
        [ContextMenu( "Stop recording" )]
		private void StopRecording() {

            Camera.main?.gameObject.SetActive( true );

            for( int i = 0; i < camera_recorders.Length; i++ ) { 
                
                camera_recorders[i].gameObject.SetActive( false );

                Debug.Log( "Recorded video has been saved for " + camera_recorders[i].name );
            }			
		}
	}
    #endif
}